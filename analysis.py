import json
import pandas as pd
from prophet import Prophet
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

# Fetching historical data
with open('response4.json', 'r') as file:
    data = json.load(file)

# Extract US data and convert to DataFrame
us_data = data['us']
df = pd.DataFrame(us_data)

# Converting Unix epoch time to datetime
df['time'] = pd.to_datetime(df['time'], unit='s')

# Rename columns to fit Prophet's requirements
df.rename(columns={'time': 'ds', 'price': 'y'}, inplace=True)

# Initialize and fit model
m = Prophet()
m.fit(df)

# Define future timeframe
start_date = datetime.now() + timedelta(days=1)
end_date = datetime(2023, 8, 13)
future_dates = pd.date_range(start=start_date, end=end_date)

future = pd.DataFrame(future_dates, columns=['ds'])

# Predict
forecast = m.predict(future)

# Print predictions for the specified period
forecast_filtered = forecast[(forecast['ds'] >= start_date) & (forecast['ds'] <= end_date)]
print(forecast_filtered[['ds', 'yhat', 'yhat_lower', 'yhat_upper']])

# Plotting forecast
fig1 = m.plot(forecast)
plt.show(block=True)

# Plotting forecast components
fig2 = m.plot_components(forecast)
plt.show(block=True)
